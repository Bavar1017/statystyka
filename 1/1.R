# population size
N<-910000;
# Random populations
P1 <- rnorm(N, 10, 99*N);
P2 <- rnorm(N, 10, 99*N);
# sample size
RozmProb <- 300000;
# samples
S1 <- sample(P1, RozmProb);
S2 <- sample(P2, RozmProb);
## t.student test
t.test(S1,S2)